$ErrorActionPreference = "Stop"
. "$PSScriptRoot\functions.ps1"

$consul_address = $env:bamboo_consul_address
if ($env:bamboo_hiscox_environment) {
  $hiscox_environment = $env:bamboo_hiscox_environment
} else {
  $hiscox_environment = $env:bamboo_deploy_environment
}
$components = @("portal", "orchestrator", "contentservices", "integration", "solr", "forms")
$jobs = @()

foreach ($component in $components) {
  Write-Host "Stopping puppet on ${component}"
  Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
    $session = New-PSSession -ComputerName $_
    $jobs += Invoke-Command -Session $session -ScriptBlock {
      while (@((Get-CimInstance Win32_Process | Where-Object name -eq "ruby.exe").CommandLine) -match "puppet agent" -ne $null) {
        Start-Sleep -Seconds 10
      }
      Stop-Service puppet
      Stop-Service pxp-agent
    } -AsJob
  }
}

$jobs | Receive-Job -Wait