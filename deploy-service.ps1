param
(
    [string]$service_name,
    [boolean]$rename_context_path = $False
)

$ErrorActionPreference = "Stop"
. "$PSScriptRoot\functions.ps1"

$consul_address = $env:bamboo_consul_address
if ($env:bamboo_hiscox_environment) {
    $hiscox_environment = $env:bamboo_hiscox_environment
} else {
    $hiscox_environment = $env:bamboo_deploy_environment
}

$components = @("integration")
$jobs = @()

foreach ($component in $components) {
    switch ($component) {
        "integration" {
            Write-Host "Deploying ${service_name} on ${component}"
            Push-Location ".\${component}"
            Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
                $session = New-PSSession -ComputerName $_
                $wardestination = "C:\programs\tomcat\apache-tomcat-8.0.18\${component}\webapps"

                Invoke-Command -Session $session -ScriptBlock {
                    $service_name = $using:service_name
                    $wardestination = $using:wardestination
                    Get-ChildItem $wardestination |
                            Where{$_.Name -Match ".*${service_name}.*"} |
                            Remove-Item -Recurse -Force -ErrorAction Ignore
                }
                $destination = "${wardestination}\${service_name}.war"
                if ($rename_context_path) {
                    $destination = "${wardestination}\${component}#${service_name}.war"
                }
                Get-ChildItem .\ -Recurse |
                        Where{$_.Name -Match ".*${service_name}.*.war"} |
                        Copy-Item -ToSession $session -Destination $destination -Force
            }
            Pop-Location
        }
        Default {}
    }
}

$jobs | Receive-Job -Wait

