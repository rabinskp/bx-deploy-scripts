$ErrorActionPreference = "Stop"
. "$PSScriptRoot\functions.ps1"

$consul_address = $env:bamboo_consul_address
if ($env:bamboo_hiscox_environment) {
  $hiscox_environment = $env:bamboo_hiscox_environment
} else {
  $hiscox_environment = $env:bamboo_deploy_environment
}

$sql_user = $env:bamboo_sql_user
$sql_password = $env:bamboo_sql_password

$sql_alias = $env:bamboo_idit_environment + "-idit.azure.hiscox.com"

Write-Host "Preparing transient > generated scripts..." $sql_alias

Set-Location ".\db"
$db_resources_path = (Get-Location).Path

Write-Host "SQL alias: " $sql_alias
Write-Host "DB resources path: " $db_resources_path

Set-Location ".\transient"

$result = psexec \\localhost -accepteula -user $sql_user -password $sql_password -c "execute.bat" $db_resources_path $sql_alias
$result | Out-File .\execute-out.log
$result | Write-Host
if ( ("" + $result) -notlike "*transient*")
{
    throw "An error has happened while executing the transient scripts."
}

Set-Location "..\.."
