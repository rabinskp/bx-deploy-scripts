$ErrorActionPreference = "Stop"
. "$PSScriptRoot\functions.ps1"

$consul_address = $env:bamboo_consul_address
if ($env:bamboo_hiscox_environment) {
  $hiscox_environment = $env:bamboo_hiscox_environment
} else {
  $hiscox_environment = $env:bamboo_deploy_environment
}

$portal_address = Get-NodeAddress $hiscox_environment "portal" $consul_address | Select-Object -First 1
$integration_address = Get-NodeAddress $hiscox_environment "integration" $consul_address | Select-Object -First 1
$integration_node = Get-NodeName $hiscox_environment "integration" $consul_address "azure.hiscox.com" | Select-Object -First 1

Write-Host "Running imports on ${integration_node}"
$session = New-PSSession -ComputerName $integration_node
Invoke-Command -Session $session -FilePath "$PSScriptRoot\functions.ps1"
Invoke-Command -Session $session -ScriptBlock {
  $portal_address = $using:portal_address
  $portal_host = $portal_address.Split(":")[0]
  $portal_port = $portal_address.Split(":")[1]
  $dateTime = Get-Date -UFormat "%Y%m%d%H%M"
  $logFile = "C:\programs\tomcat\apache-tomcat-8.0.18\integration\logs\imports_$dateTime.log"
  Initialize-Portal $portal_host $portal_port
  Import-Bundle $portal_host $portal_port "C:\programs\tomcat\apache-tomcat-8.0.18\integration\imports"
  Import-PortalExport $portal_host $portal_port "C:\programs\tomcat\apache-tomcat-8.0.18\integration\imports"
  $integration_address = $using:integration_address
  $integration_host = $integration_address.Split(":")[0]
  $integration_port = $integration_address.Split(":")[1]
  Import-ContentExport $integration_host $integration_port "C:\programs\tomcat\apache-tomcat-8.0.18\integration\imports"
}