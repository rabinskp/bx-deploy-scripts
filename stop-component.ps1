param
(
    [string]$component
)

$ErrorActionPreference = "Stop"
. "$PSScriptRoot\functions.ps1"

$consul_address = $env:bamboo_consul_address
if ($env:bamboo_hiscox_environment) {
    $hiscox_environment = $env:bamboo_hiscox_environment
} else {
    $hiscox_environment = $env:bamboo_deploy_environment
}

$jobs = @()

Write-Host "Stopping ${component}"
Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
    $session = New-PSSession -ComputerName $_
    $jobs += Invoke-Command -Session $session -ScriptBlock {
        $component = $using:component
        $service = Get-CimInstance -ClassName Win32_Service -Filter "Name = '$component'"
        if ($service.State -ne "Stopped") {
            Stop-Process -Id $service.ProcessId -Force
        }
    } -AsJob
}

$jobs | Receive-Job -Wait