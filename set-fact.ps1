$ErrorActionPreference = "Stop"
. "$PSScriptRoot\functions.ps1"

$consul_address = $env:bamboo_consul_address
if ($env:bamboo_hiscox_environment) {
  $hiscox_environment = $env:bamboo_hiscox_environment
} else {
  $hiscox_environment = $env:bamboo_deploy_environment
}
$release = $env:bamboo_deploy_release
$components = @("portal", "orchestrator", "contentservices", "integration", "solr", "forms")
$jobs = @()

foreach ($component in $components) {
  Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
    $session = New-PSSession -ComputerName $_
    $jobs += Invoke-Command -Session $session -ScriptBlock {
      $release = $using:release
      # TEMP remove old fact
      if (Test-Path "C:\ProgramData\PuppetLabs\facter\facts.d\backbase_version.ps1") {
        Remove-Item "C:\ProgramData\PuppetLabs\facter\facts.d\backbase_version.ps1" -Force
      }
      "Write-Output 'backbase_release=${release}'" | Set-Content "C:\ProgramData\PuppetLabs\facter\facts.d\backbase_release.ps1" -Force
      & "C:\Program Files\Puppet Labs\Puppet\bin\puppet.bat" agent --test 2>&1 | ForEach-Object {
        if (-not($_.ToString() -like "*warning*")) { "$env:COMPUTERNAME - $_" }
      }
      Start-Service puppet
      Start-Service pxp-agent
    } -AsJob
  }
}

$jobs | Receive-Job -Wait