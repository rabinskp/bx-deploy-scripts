$ErrorActionPreference = "Stop"
. "$PSScriptRoot\functions.ps1"

$consul_address = $env:bamboo_consul_address
$hiscox_environment = $env:bamboo_hiscox_environment
$release_version = $env:bamboo_deploy_release
$components = @("portal","integration","forms")
$jobs = @()

foreach ($component in $components) {
  Write-Host "Stopping ${component}"
  Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
    $session = New-PSSession -ComputerName $_
    $jobs += Invoke-Command -Session $session -ScriptBlock {
      param($release_version, $component)
      $packages = switch ($component) {
        "portal"      {"cxp cxp-config"}
        "integration" {"integration integration-config imports"}
        "forms"       {"forms forms-config"}
      }
      & cmd.exe /c "choco upgrade ${packages} --version ${release_version} -y --ignorechecksum --allowdowngrade"
    } -ArgumentList ($release_version, $component) -AsJob
  }
}

$jobs | Receive-Job -Wait
