$ErrorActionPreference = "Stop"
. "$PSScriptRoot\functions.ps1"

$consul_address = $env:bamboo_consul_address
if ($env:bamboo_hiscox_environment) {
  $hiscox_environment = $env:bamboo_hiscox_environment
} else {
  $hiscox_environment = $env:bamboo_deploy_environment
}
$jobs = @()

& 7z.exe a imports.7z .\imports
Get-NodeName $hiscox_environment "integration" $consul_address "azure.hiscox.com" | ForEach-Object {
  $session = New-PSSession -ComputerName $_
  Copy-Item ".\imports.7z" -ToSession $session -Destination "C:\programs\tomcat\apache-tomcat-8.0.18\integration"
  $jobs += Invoke-Command -Session $session -ScriptBlock {
    Set-Location "C:\programs\tomcat\apache-tomcat-8.0.18\integration"
    if (Test-Path ".\imports") {
      Remove-Item ".\imports" -Recurse -Force
    }
    & 7z.exe x .\imports.7z
  } -AsJob
}

$jobs | Receive-Job -Wait