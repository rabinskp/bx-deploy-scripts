$ErrorActionPreference = "Stop"
. "$PSScriptRoot\functions.ps1"

$consul_address = $env:bamboo_consul_address
if ($env:bamboo_hiscox_environment) {
  $hiscox_environment = $env:bamboo_hiscox_environment
} else {
  $hiscox_environment = $env:bamboo_deploy_environment
}
$components = @("portal", "orchestrator", "contentservices", "integration", "solr", "forms")

foreach ($component in $components) {
  switch ($component) {
    "integration" {
      Push-Location "."
      Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
        $session = New-PSSession -ComputerName $_
        # TODO make this a tag on the consul service definition
        $dbdestination = "C:\programs\tomcat\apache-tomcat-8.0.18\$component\db"
        Invoke-Command -Session $session -ScriptBlock {
          $livepath = Join-Path $using:dbdestination "live"
          $transientpath = Join-Path $using:dbdestination "transient"
          $staticpath = Join-Path $using:dbdestination "static"
          $generatedpath = Join-Path $using:dbdestination "generated"
          Get-ChildItem $livepath -Recurse | Remove-Item -Recurse -Force -Verbose
          Get-ChildItem $transientpath -Recurse | Remove-Item -Recurse -Force -Verbose
          Get-ChildItem $staticpath -Recurse | Remove-Item -Recurse -Force -Verbose
          Get-ChildItem $generatedpath -Recurse | Remove-Item -Recurse -Force -Verbose
        }
        Get-ChildItem "live" -Directory -Recurse -Verbose | Get-ChildItem -Recurse -Verbose | Copy-Item -ToSession $session -Destination "$dbdestination\live" -Verbose
        Get-ChildItem "transient" -Directory -Recurse -Verbose | Get-ChildItem -Recurse -Verbose | Copy-Item -ToSession $session -Destination "$dbdestination\transient" -Verbose
        Get-ChildItem "static" -Directory -Recurse -Verbose | Get-ChildItem -Recurse -Verbose | Copy-Item -ToSession $session -Destination "$dbdestination\static" -Verbose
        Get-ChildItem "generated" -Directory -Recurse -Verbose | Get-ChildItem -Recurse -Verbose | Copy-Item -ToSession $session -Destination "$dbdestination\generated" -Verbose
      }
      Pop-Location
    }
    Default {}
  }
}