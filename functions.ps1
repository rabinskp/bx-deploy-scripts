$ErrorActionPreference = "Stop"
function Get-NodeAddress ($hiscox_environment, $component, $consul_address) {
  $service_name = "${hiscox_environment}-backbase-${component}"
  $url = "http://${consul_address}:8500/v1/health/service/${service_name}"
  $nodes = @()
  $nodes += Invoke-RestMethod $url
  $nodes.Foreach({
    Write-Output "$($_.Service.Address):$($_.Service.Port)"
  })
}

function Get-NodeName ($hiscox_environment, $component, $consul_address, $dns_suffix) {
  $service_name = "${hiscox_environment}-backbase-${component}"
  $url = "http://${consul_address}:8500/v1/health/service/${service_name}"
  $nodes = @()
  $nodes += Invoke-RestMethod $url
  $nodes.Foreach({
    Write-Output "$($_.Node.Node).${dns_suffix}"
  })
}

function Wait-ConsulService ($hiscox_environment, $component, $consul_address, $timeout = 360) {
  $maxtries = [math]::Round($timeout / 15)
  $tries = 0
  $service_name = "${hiscox_environment}-backbase-${component}"
  $url = "http://${consul_address}:8500/v1/health/checks/${service_name}"
  $health = Invoke-RestMethod $url
  $failing = @($health | Where-Object Status -ne 'passing')
  while ($failing.Count -gt 0 -and $tries -lt $maxtries) {
    Write-Host "Waiting for ${service_name}"
    Start-Sleep -Seconds 30
    $health = Invoke-RestMethod $url
    $failing = @($health | Where-Object Status -ne 'passing')
    $tries++
  }
  if ($tries -ge $maxtries) {
    throw "Timeout exceeded waiting for ${service_name}"
  }
}

function Deploy-War ($address, $component, $credential) {
  # $result = Invoke-RestMethod -Uri "http://${address}/manager/text/deploy?path=/$component" -Method Put -InFile ".\${component}.war" -Credential $credential -Verbose -DisableKeepAlive -UseBasicParsing
  # Write-Host $result
  # if ($result.ToString() -notlike "OK - Deployed application*") {
  #   throw $result.ToString()
  # }
  $env:HTTP_PROXY = ""
  $env:HTTPS_PROXY = ""
  $job = Start-Job -ScriptBlock {
    param (
      $location, $address, $component, $credential
    )
    Set-Location $location
    $result = & curl.exe -u "$($credential.UserName):$($credential.GetNetworkCredential().Password)" --upload-file ".\${component}.war" "http://${address}/manager/text/deploy?path=/$component" -s
    if ($result.ToString() -notlike "OK - Deployed application*") {
      throw "Failed to deploy $component to $address - $($result.ToString())"
    }
  } -ArgumentList (Get-Location).Path,$address,$component,$credential
  return $job
}

function Deploy-War2 ($address, $component, $war, $credential) {
  # $result = Invoke-RestMethod -Uri "http://${address}/manager/text/deploy?path=/$war" -Method Put -InFile ".\${war}.war" -Credential $credential -Verbose -DisableKeepAlive -UseBasicParsing
  # Write-Host $result
  # if ($result.ToString() -notlike "OK - Deployed application*") {
  #   throw $result.ToString()
  # }
  $env:HTTP_PROXY = ""
  $env:HTTPS_PROXY = ""
  $job = Start-Job -ScriptBlock {
    param (
      $location, $address, $component, $war, $credential
    )
    Set-Location $location
    $result = & curl.exe -u "$($credential.UserName):$($credential.GetNetworkCredential().Password)" --upload-file ".\${war}.war" "http://${address}/manager/text/deploy?path=/$war" -s
    if ($result.ToString() -notlike "OK - Deployed application*") {
      throw "Failed to deploy $component to $address - $($result.ToString())"
    }
  } -ArgumentList (Get-Location).Path,$address,$component,$war,$credential
  return $job
}

function Initialize-Portal ($portal_host, $portal_port) {
  "Removing dashboard" | Out-File $logFile -Append
  & "C:\programs\bb-tools-dist\bb.cmd" rest -p dashboard -t portal -T dashboard -H $portal_host -P $portal_port -c portal -m delete --verbose |
    Out-File $logFile -Append
  "Importing dashboard" | Out-File $logFile -Append
  & "C:\programs\bb-tools-dist\bb.cmd" import --dashboard -H $portal_host -P $portal_port -c portal --verbose |
    ForEach-Object { if ($_.Contains("Error:")) { throw $_ } else { Write-Output $_ } } | Out-File $logFile -Append
  Get-ChildItem "C:\programs\bb-tools-dist\bower_components" | ForEach-Object {
    "Importing collection: $_" | Out-File $logFile -Append
    & "C:\programs\bb-tools-dist\bb.cmd" import-item -t $_.FullName -H $portal_host -P $portal_port -c portal --verbose |
      ForEach-Object { if ($_.Contains("Error:")) { throw $_ } else { Write-Output $_ } } | Out-File $logFile -Append
  }
}

function Import-Bundle ($portal_host, $portal_port, $import_path) {
  "templates", "features", "containers", "widgets" | ForEach-Object {
    "Importing bundle: $_" | Out-File $logFile -Append
    Get-ChildItem "$import_path\bundles\static\hiscox-statics\$_" | ForEach-Object {
      "---Importing: $_" | Out-File $logFile -Append
      & "C:\programs\bb-tools-dist\bb.cmd" import-item -t $_.FullName -H $portal_host -P $portal_port -c portal --verbose |
        ForEach-Object { if ($_.Contains("Error:")) { throw $_ } else { Write-Output $_ } } | Out-File $logFile -Append
    }
  }
}

function Import-PortalExport ($portal_host, $portal_port, $import_path) {
  "Importing portal export" | Out-File $logFile -Append
  & "C:\programs\bb-tools-dist\bb.cmd" rest -p hiscox -t portal -T hiscox -H $portal_host -P $portal_port -c portal -x --verbose |
    ForEach-Object { if ($_.Contains("Error:")) { throw $_ } else { Write-Output $_ } } | Out-File $logFile -Append
  & "C:\programs\bb-tools-dist\bb.cmd" rest -p hiscox -t portal -T hiscox -H $portal_host -P $portal_port -c portal -m delete --verbose |
    ForEach-Object { if ($_.Contains("Error:")) {
      if ($_.Contains("Error: 404 Not Found")) { Write-Output $_ } else { throw $_ }
    } else { Write-Output $_ } } | Out-File $logFile -Append
  & "C:\programs\bb-tools-dist\bb.cmd" import -t "$import_path\cxp-exports\hiscox" -H $portal_host -P $portal_port -c portal --verbose |
    ForEach-Object { if ($_.Contains("Error:")) { throw $_ } else { Write-Output $_ } } | Out-File $logFile -Append
}

function Import-ContentExport ($integration_host, $integration_port, $import_path) {
  $data = @"
------
Content-Disposition: form-data; name="tempFileLocation"

$import_path
------
Content-Disposition: form-data; name="archiveName"

contentservices.zip
------
Content-Disposition: form-data; name="contentRoot"

/
------
Content-Disposition: form-data; name="ignoreList"


------
Content-Disposition: form-data; name="imageCmisType"

bb:image
------
Content-Disposition: form-data; name="textCmisType"

bb:richtext
------
"@

  $url = "http://${integration_host}:${integration_port}/integration/rest/contentServices/import"
  $user = "admin"
  $pass = "admin"
  $secpasswd = ConvertTo-SecureString $pass -AsPlainText -Force
  $cred = New-Object System.Management.Automation.PSCredential($user, $secpasswd)
  "Importing content export" | Out-File $logFile -Append
  $result = Invoke-RestMethod -Uri $url -Method Post -body $data -Credential $cred -ContentType "application/json"
  $result.result | Out-File $logFile -Append
  if ($result.result -ne "SUCCESS")
  {
    throw $result.result
  }
}

function DB-ExecuteSQLGoals ($integration_host, $integration_port, $operation, $goals, $location) {
  $data = ConvertTo-Json @{
      operation = $operation
      goals = $goals
      sourceLocation = $location
      targetLocation = $location
  }

  $url = "http://${integration_host}:${integration_port}/integration/rest/SQLInterface/executeSQLGoals"
  $user = "admin"
  $pass = "admin"
  $secpasswd = ConvertTo-SecureString $pass -AsPlainText -Force
  $cred = New-Object System.Management.Automation.PSCredential($user, $secpasswd)
  Write-Host "Executing SQL operation/goals: $operation/$goals"
  $result = Invoke-RestMethod -Uri $url -Method Post -body $data -Credential $cred -ContentType "application/json"
  $result.result | Out-File $logFile -Append
  Write-Host $result.result
  if ($result.result -contains "ERROR")
  {
    throw $result.result
  }
}

function DB-ExecuteSQLGoals2 ($integration_host, $integration_port, $operation, $goals, $location, $logFile) {
  $data = ConvertTo-Json @{
      operation = $operation
      goals = $goals
      sourceLocation = $location
      targetLocation = $location
  }
  $url = "http://${integration_host}:${integration_port}/integration/rest/SQLInterface/executeSQLGoals"
  $user = "admin"
  $pass = "admin"
  $secpasswd = ConvertTo-SecureString $pass -AsPlainText -Force
  $cred = New-Object System.Management.Automation.PSCredential($user, $secpasswd)
  $pair = "${user}:${pass}"
  $bytes = [System.Text.Encoding]::ASCII.GetBytes($pair)
  $base64 = [System.Convert]::ToBase64String($bytes)
  $auth = "Basic $base64"
  $header = @{Authorization = $auth; "Accept" = "application/json"}
  Write-Host "Executing SQL operation/goals: $operation/$goals"
  $result = Invoke-RestMethod -Uri $url -Method Post -ContentType "application/json" -Header $header -Body $data -Credential $cred -UseBasicParsing -Verbose
    #$result = & curl.exe -u "${user}:${secpasswd}" -H "Content-Type: application/json" -X POST "${url}" -d $data
  $result.result | Out-File $logFile -Append
  Write-Host $result.result
  if ($result.result -contains "ERROR")
    {
        throw $result.result
    }
}

function DB-ExecuteSQLGoalsForWebDB ($integration_host, $integration_port, $operation, $goals, $location, $logFile) {
  $data = ConvertTo-Json @{
      operation = $operation
      goals = $goals
      sourceLocation = $location
      targetLocation = $location
  }
  $url = "http://${integration_host}:${integration_port}/web-db/SQLInterface/executeSQLGoals"
  $user = "admin"
  $pass = "admin"
  $secpasswd = ConvertTo-SecureString $pass -AsPlainText -Force
  $cred = New-Object System.Management.Automation.PSCredential($user, $secpasswd)
  $pair = "${user}:${pass}"
  $bytes = [System.Text.Encoding]::ASCII.GetBytes($pair)
  $base64 = [System.Convert]::ToBase64String($bytes)
  $auth = "Basic $base64"
  $header = @{Authorization = $auth; "Accept" = "application/json"}
  Write-Host "Executing SQL operation/goals: $operation/$goals"
  $result = Invoke-RestMethod -Uri $url -Method Post -ContentType "application/json" -Header $header -Body $data -Credential $cred -UseBasicParsing -Verbose
    #$result = & curl.exe -u "${user}:${secpasswd}" -H "Content-Type: application/json" -X POST "${url}" -d $data
  $result.result | Out-File $logFile -Append
  Write-Host $result.result
  if ($result.result -contains "ERROR")
    {
        throw $result.result
    }
}


function ClearDAOCache ($integration_address) {
  $integration_host = $integration_address.Split(":")[0]
  $integration_port = $integration_address.Split(":")[1]
  $data = ""
  $url = "http://${integration_host}:${integration_port}/integration/rest/SQLInterface/clearDAOCache"
  $user = "admin"
  $pass = "admin"
  $secpasswd = ConvertTo-SecureString $pass -AsPlainText -Force
  $cred = New-Object System.Management.Automation.PSCredential($user, $secpasswd)
  $pair = "${user}:${pass}"
  $bytes = [System.Text.Encoding]::ASCII.GetBytes($pair)
  $base64 = [System.Convert]::ToBase64String($bytes)
  $auth = "Basic $base64"
  $header = @{Authorization = $auth; "Accept" = "application/json"}
  Write-Host "Executing clearDAOCache:"
  $result = Invoke-RestMethod -Uri $url -Method Post -ContentType "application/json" -Header $header -Body $data -Credential $cred -UseBasicParsing -Verbose
    #$result = & curl.exe -u "${user}:${secpasswd}" -H "Content-Type: application/json" -X POST "${url}" -d $data
  Write-Host $result.result
  if ($result.result -contains "ERROR")
    {
        throw $result.result
    }
}

function ClearRepositoryCache ($integration_address) {
  $integration_host = $integration_address.Split(":")[0]
  $integration_port = $integration_address.Split(":")[1]
  $data = ""
  $url = "http://${integration_host}:${integration_port}/integration/rest/SQLInterface/clearRepositoryCache"
  $user = "admin"
  $pass = "admin"
  $secpasswd = ConvertTo-SecureString $pass -AsPlainText -Force
  $cred = New-Object System.Management.Automation.PSCredential($user, $secpasswd)
  $pair = "${user}:${pass}"
  $bytes = [System.Text.Encoding]::ASCII.GetBytes($pair)
  $base64 = [System.Convert]::ToBase64String($bytes)
  $auth = "Basic $base64"
  $header = @{Authorization = $auth; "Accept" = "application/json"}
  Write-Host "Executing clearDAOCache:"
  $result = Invoke-RestMethod -Uri $url -Method Post -ContentType "application/json" -Header $header -Body $data -Credential $cred -UseBasicParsing -Verbose
    #$result = & curl.exe -u "${user}:${secpasswd}" -H "Content-Type: application/json" -X POST "${url}" -d $data
  Write-Host $result.result
  if ($result.result -contains "ERROR")
    {
        throw $result.result
    }
}

function ClearBAPCache ($integration_address) {
  $integration_host = $integration_address.Split(":")[0]
  $integration_port = $integration_address.Split(":")[1]
  $data = ""
  $url = "http://${integration_host}:${integration_port}/integration/rest/bapSearch/clearCache"
  $user = "admin"
  $pass = "admin"
  $secpasswd = ConvertTo-SecureString $pass -AsPlainText -Force
  $cred = New-Object System.Management.Automation.PSCredential($user, $secpasswd)
  $pair = "${user}:${pass}"
  $bytes = [System.Text.Encoding]::ASCII.GetBytes($pair)
  $base64 = [System.Convert]::ToBase64String($bytes)
  $auth = "Basic $base64"
  $header = @{Authorization = $auth; "Accept" = "application/json"}
  Write-Host "Executing clearDAOCache:"
  $result = Invoke-RestMethod -Uri $url -Method Post -ContentType "application/json" -Header $header -Body $data -Credential $cred -UseBasicParsing -Verbose
    #$result = & curl.exe -u "${user}:${secpasswd}" -H "Content-Type: application/json" -X POST "${url}" -d $data
  Write-Host $result.result
  if ($result.result -contains "ERROR")
    {
        throw $result.result
    }
}
