$ErrorActionPreference = "Stop"
. "$PSScriptRoot\functions.ps1"

$consul_address = $env:bamboo_consul_address
if ($env:bamboo_hiscox_environment) {
  $hiscox_environment = $env:bamboo_hiscox_environment
} else {
  $hiscox_environment = $env:bamboo_deploy_environment
}
$components = @("solr", "portal", "contentservices", "orchestrator", "integration", "forms")
$jobs = @()
# all of these sessions blocks should be converted into generic functions in functions.ps1!!!!
foreach ($component in $components) {
  switch ($component) {
    "solr" {
      Write-Host "Deploying ${component}"
      Push-Location ".\$component"
      Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
        $session = New-PSSession -ComputerName $_
        $wardestination = "C:\programs\tomcat\apache-tomcat-8.0.18\${component}\webapps"
        Invoke-Command -Session $session -Command {
          $wardestination = $using:wardestination
          Remove-Item $wardestination\* -Recurse -Force
        }
        Get-ChildItem "*.war" -Recurse | Copy-Item -ToSession $session -Destination $wardestination -Force
      }
      Pop-Location
    }
    "portal" {
      Write-Host "Deploying ${component}"
      Push-Location ".\$component"
      Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
        $session = New-PSSession -ComputerName $_
        $wardestination = "C:\programs\tomcat\apache-tomcat-8.0.18\${component}\webapps"
        Invoke-Command -Session $session -ScriptBlock {
          $wardestination = $using:wardestination
          Remove-Item $wardestination\* -Recurse -Force
        }
        Get-ChildItem "*.war" -Recurse | Copy-Item -ToSession $session -Destination $wardestination -Force
      }
      Pop-Location
    }
    "contentservices" {
      Write-Host "Deploying ${component}"
      Push-Location ".\${component}"
      Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
        $session = New-PSSession -ComputerName $_
        $wardestination = "C:\programs\tomcat\apache-tomcat-8.0.18\${component}\webapps"
        Invoke-Command -Session $session -ScriptBlock {
          $wardestination = $using:wardestination
          Remove-Item $wardestination\* -Recurse -Force
        }
        Get-ChildItem "*.war" -Recurse | Copy-Item -ToSession $session -Destination $wardestination -Force
      }
      Pop-Location
    }
    "orchestrator" {
      Write-Host "Deploying ${component}"
      Push-Location ".\${component}"
      Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
        $session = New-PSSession -ComputerName $_
        $wardestination = "C:\programs\tomcat\apache-tomcat-8.0.18\${component}\webapps"
        Invoke-Command -Session $session -ScriptBlock {
          $wardestination = $using:wardestination
          Remove-Item $wardestination\* -Recurse -Force
        }
        Get-ChildItem "*.war" -Recurse | Copy-Item -ToSession $session -Destination $wardestination -Force
      }
      Pop-Location
    }
    "integration" {
      Write-Host "Deploying ${component}"
      Push-Location ".\${component}"
      Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
        $session = New-PSSession -ComputerName $_
        $wardestination = "C:\programs\tomcat\apache-tomcat-8.0.18\${component}\webapps"
        $integration_wars = Get-ChildItem "*.war" | Foreach-Object {$_.BaseName}
        Invoke-Command -Session $session -ScriptBlock {
          $wardestination = $using:wardestination
          $integration_wars = $using:integration_wars
          foreach ($war_name in $integration_wars) {
            Remove-Item "${wardestination}\${war_name}" -Recurse -Force -ErrorAction Ignore
            Remove-Item "${wardestination}\${war_name}.war" -Recurse -Force -ErrorAction Ignore
          }
        }
        Get-ChildItem "*.war" -Recurse | Copy-Item -ToSession $session -Destination $wardestination -Force
      }
      Pop-Location
    }
    "forms" {
      Write-Host "Deploying ${component}"
      Push-Location ".\${component}"
      # Rename-Item ".\forms-runtime.war" -NewName "forms.war"
      Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
        $session = New-PSSession -ComputerName $_
        $wardestination = "C:\programs\tomcat\apache-tomcat-8.0.18\${component}\webapps"
        Invoke-Command -Session $session -ScriptBlock {
          $wardestination = $using:wardestination
          Remove-Item $wardestination\* -Recurse -Force
        }
        Get-ChildItem "*.war" -Recurse | Copy-Item -ToSession $session -Destination $wardestination -Force
      }
      Pop-Location
    }
    Default {}
  }
}

$jobs | Receive-Job -Wait