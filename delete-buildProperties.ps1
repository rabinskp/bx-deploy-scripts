$ErrorActionPreference = "Stop"
. "$PSScriptRoot\functions.ps1"

$consul_address = $env:bamboo_consul_address
if ($env:bamboo_hiscox_environment) {
  $hiscox_environment = $env:bamboo_hiscox_environment
} else {
  $hiscox_environment = $env:bamboo_deploy_environment
}
$components = @("portal", "orchestrator", "contentservices", "integration", "solr", "forms")

foreach ($component in $components) {
  switch ($component) {
    "portal" {
      Push-Location "."
      Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
        $session = New-PSSession -ComputerName $_
        Invoke-Command -Session $session -ScriptBlock {
          $path = "C:\programs\build.properties"
          if (Test-Path $path) {
            Remove-Item -Path $path -Force -Verbose
          }
        }
      }
      Pop-Location
    }
    "orchestrator" {
      Push-Location "."
      Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
        $session = New-PSSession -ComputerName $_
        Invoke-Command -Session $session -ScriptBlock {
          $path = "C:\programs\build.properties"
          if (Test-Path $path) {
            Remove-Item -Path $path -Force -Verbose
          }
        }
      }
      Pop-Location
    }
    "contentservices" {
      Push-Location "."
      Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
        $session = New-PSSession -ComputerName $_
        Invoke-Command -Session $session -ScriptBlock {
          $path = "C:\programs\build.properties"
          if (Test-Path $path) {
            Remove-Item -Path $path -Force -Verbose
          }
        }
      }
      Pop-Location
    }
    "integration" {
      Push-Location "."
      Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
        $session = New-PSSession -ComputerName $_
        Invoke-Command -Session $session -ScriptBlock {
          $path = "C:\programs\build.properties"
          if (Test-Path $path) {
            Remove-Item -Path $path -Force -Verbose
          }
        }
      }
      Pop-Location
    }
    "solr" {
      Push-Location "."
      Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
        $session = New-PSSession -ComputerName $_
        Invoke-Command -Session $session -ScriptBlock {
          $path = "C:\programs\build.properties"
          if (Test-Path $path) {
            Remove-Item -Path $path -Force -Verbose
          }
        }
      }
      Pop-Location
    }
    "forms" {
      Push-Location "."
      Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
        $session = New-PSSession -ComputerName $_
        Invoke-Command -Session $session -ScriptBlock {
          $path = "C:\programs\build.properties"
          if (Test-Path $path) {
            Remove-Item -Path $path -Force -Verbose
          }
        }
      }
      Pop-Location
    }
    Default {}
  }
}