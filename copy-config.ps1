$ErrorActionPreference = "Stop"
. "$PSScriptRoot\functions.ps1"

$consul_address = $env:bamboo_consul_address
if ($env:bamboo_hiscox_environment) {
  $hiscox_environment = $env:bamboo_hiscox_environment
} else {
  $hiscox_environment = $env:bamboo_deploy_environment
}
$components = @("portal", "orchestrator", "contentservices", "integration", "solr", "forms")

foreach ($component in $components) {
  switch ($component) {
    "portal" {
      Push-Location ".\$component"
      Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
        $session = New-PSSession -ComputerName $_
        # TODO make this a tag on the consul service definition
        $confdestination = "C:\programs\tomcat\apache-tomcat-8.0.18\$component\conf"
        Copy-Item "..\fixed-context\${component}.xml" ".\context.xml" -Force
        Get-ChildItem "*.xml" -Recurse | Copy-Item -ToSession $session -Destination $confdestination -Force
      }
      Pop-Location
    }
    "orchestrator" {
      Push-Location ".\$component"
      Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
        $session = New-PSSession -ComputerName $_
        # TODO make this a tag on the consul service definition
        $confdestination = "C:\programs\tomcat\apache-tomcat-8.0.18\$component\conf"
        Copy-Item "..\fixed-context\${component}.xml" ".\context.xml" -Force
        Get-ChildItem "*.xml" -Recurse | Copy-Item -ToSession $session -Destination $confdestination -Force
      }
      Pop-Location
    }
    "contentservices" {
      Push-Location ".\$component"
      Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
        $session = New-PSSession -ComputerName $_
        # TODO make this a tag on the consul service definition
        $confdestination = "C:\programs\tomcat\apache-tomcat-8.0.18\$component\conf"
        Copy-Item "..\fixed-context\${component}.xml" ".\context.xml" -Force
        Get-ChildItem "*.xml" -Recurse | Copy-Item -ToSession $session -Destination $confdestination -Force
      }
      Pop-Location
    }
    "integration" {
      Push-Location ".\$component"
      Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
        $session = New-PSSession -ComputerName $_
        # TODO make this a tag on the consul service definition
        $confdestination = "C:\programs\tomcat\apache-tomcat-8.0.18\$component\conf"
        Invoke-Command -Session $session -ScriptBlock {
          $dcpath = Join-Path $using:confdestination "dc"
          $dhpath = Join-Path $using:confdestination "dh"
          Get-ChildItem $dcpath -Recurse | Remove-Item -Recurse -Force
          Get-ChildItem $dhpath -Recurse | Remove-Item -Recurse -Force
        }
        Get-ChildItem "dc" -Directory -Recurse | Get-ChildItem -Recurse | Copy-Item -ToSession $session -Destination "$confdestination\dc"
        Get-ChildItem "dh" -Directory -Recurse | Get-ChildItem -Recurse | Copy-Item -ToSession $session -Destination "$confdestination\dh"
        Copy-Item "..\fixed-context\${component}.xml" ".\context.xml" -Force
        Get-ChildItem "*.xml" -Recurse | Copy-Item -ToSession $session -Destination $confdestination -Force
      }
      Pop-Location
    }
    "solr" {
      Push-Location ".\$component"
      Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
        $session = New-PSSession -ComputerName $_
        # TODO make this a tag on the consul service definition
        $confdestination = "C:\programs\tomcat\apache-tomcat-8.0.18\$component\conf"
        Get-ChildItem "solr.xml" -Recurse | Copy-Item -ToSession $session -Destination $confdestination -Force
        Copy-Item ".\solr\core0" -ToSession $session -Destination $confdestination -Recurse -Force
      }
      Pop-Location
    }
    "forms" {
      $jobs = @()
      Push-Location ".\$component"
      Remove-Item ".\forms-home\backbase.properties"
      Remove-Item ".\forms-home\aquima.properties"
      & 7z.exe a forms-home.7z .\forms-home
      Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
        $session = New-PSSession -ComputerName $_
        # TODO make this a tag on the consul service definition
        $confdestination = "C:\programs\tomcat\apache-tomcat-8.0.18\$component\conf"
        Invoke-Command -Session $session -ScriptBlock {
          $fhpath = "C:\programs\tomcat\apache-tomcat-8.0.18\forms\forms-home"
          Get-ChildItem $fhpath -Recurse -Exclude "backbase.properties","aquima.properties" | Remove-Item -Recurse -Force
        }
        Get-ChildItem "context.xml" -Recurse | Copy-Item -ToSession $session -Destination $confdestination -Force
        Copy-Item ".\forms-home.7z" -ToSession $session -Destination "C:\programs\tomcat\apache-tomcat-8.0.18\forms" -Force
        $jobs += Invoke-Command -Session $session -ScriptBlock {
          Set-Location "C:\programs\tomcat\apache-tomcat-8.0.18\forms"
          & 7z.exe x .\forms-home.7z
        } -AsJob
        $jobs | Receive-Job -Wait
        Copy-Item "..\fixed-context\${component}.xml" ".\context.xml" -Force
        Copy-Item ".\context.xml" -ToSession $session -Destination $confdestination -Force
        if (Test-Path ".\forms-home\exports\version.json") {
          Copy-Item ".\forms-home\exports\version.json" -ToSession $session -Destination "C:\ProgramData\PuppetLabs\facter\facts.d\"
        }
      }
      Pop-Location
    }
    Default {}
  }
}