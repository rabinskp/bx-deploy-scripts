$ErrorActionPreference = "Stop"
. "$PSScriptRoot\functions.ps1"

$consul_address = $env:bamboo_consul_address
if ($env:bamboo_hiscox_environment) {
  $hiscox_environment = $env:bamboo_hiscox_environment
} else {
  $hiscox_environment = $env:bamboo_deploy_environment
}
$components = @("portal", "orchestrator", "contentservices", "integration", "solr", "forms")

foreach ($component in $components) {
  switch ($component) {
    "portal" {
      Push-Location "."
      Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
        $session = New-PSSession -ComputerName $_
        $confdestination = "C:\programs"
        Get-ChildItem "build.properties" -Recurse | Copy-Item -ToSession $session -Destination $confdestination -Force
      }
      Pop-Location
    }
    "orchestrator" {
      Push-Location "."
      Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
        $session = New-PSSession -ComputerName $_
        $confdestination = "C:\programs"
        Get-ChildItem "build.properties" -Recurse | Copy-Item -ToSession $session -Destination $confdestination -Force
      }
      Pop-Location
    }
    "contentservices" {
      Push-Location "."
      Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
        $session = New-PSSession -ComputerName $_
        $confdestination = "C:\programs"
        Get-ChildItem "build.properties" -Recurse | Copy-Item -ToSession $session -Destination $confdestination -Force
      }
      Pop-Location
    }
    "integration" {
      Push-Location "."
      Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
        $session = New-PSSession -ComputerName $_
        $confdestination = "C:\programs"
        Get-ChildItem "build.properties" -Recurse | Copy-Item -ToSession $session -Destination $confdestination -Force
      }
      Pop-Location
    }
    "solr" {
      Push-Location "."
      Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
        $session = New-PSSession -ComputerName $_
        $confdestination = "C:\programs"
        Get-ChildItem "build.properties" -Recurse | Copy-Item -ToSession $session -Destination $confdestination -Force
      }
      Pop-Location
    }
    "forms" {
      Push-Location "."
      Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
        $session = New-PSSession -ComputerName $_
        $confdestination = "C:\programs"
        Get-ChildItem "build.properties" -Recurse | Copy-Item -ToSession $session -Destination $confdestination -Force
      }
      Pop-Location
    }
    Default {}
  }
}