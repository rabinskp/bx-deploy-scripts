$ErrorActionPreference = "Stop"
. "$PSScriptRoot\functions.ps1"

$consul_address = $env:bamboo_consul_address
if ($env:bamboo_hiscox_environment) {
  $hiscox_environment = $env:bamboo_hiscox_environment
} else {
  $hiscox_environment = $env:bamboo_deploy_environment
}
$componentsBase = @("portal", "orchestrator", "contentservices", "solr")
$componentsInt = @("integration")
$componentsForms = @("forms")
$jobs = @()

foreach ($component in $componentsBase) {
  $jobs = @()

  Write-Host "Starting ${component}"
  Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
    $session = New-PSSession -ComputerName $_
    Invoke-Command -Session $session -FilePath "$PSScriptRoot\functions.ps1"
    $jobs += Invoke-Command -Session $session -ScriptBlock {
      $component = $using:component
      $service = Get-CimInstance -ClassName Win32_Service -Filter "Name = '$component'"
      if ($service.State -ne "Running") {
        Start-Service $component
      }
    } -AsJob
  }

  $jobs | Receive-Job -Wait
}
Wait-ConsulService $hiscox_environment "portal" $consul_address
Wait-ConsulService $hiscox_environment "contentservices" $consul_address
Wait-ConsulService $hiscox_environment "orchestrator" $consul_address

foreach ($component in $componentsInt) {
  $jobs = @()

  Write-Host "Starting ${component}"
  Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
    $session = New-PSSession -ComputerName $_
    Invoke-Command -Session $session -FilePath "$PSScriptRoot\functions.ps1"
    $jobs += Invoke-Command -Session $session -ScriptBlock {
      $component = $using:component
      $service = Get-CimInstance -ClassName Win32_Service -Filter "Name = '$component'"
      if ($service.State -ne "Running") {
        Start-Service $component
      }
    } -AsJob
  }

  $jobs | Receive-Job -Wait
}
Wait-ConsulService $hiscox_environment "integration" $consul_address

foreach ($component in $componentsForms) {
  $jobs = @()

  Write-Host "Starting ${component}"
  Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
    $session = New-PSSession -ComputerName $_
    Invoke-Command -Session $session -FilePath "$PSScriptRoot\functions.ps1"
    $jobs += Invoke-Command -Session $session -ScriptBlock {
      $component = $using:component
      $service = Get-CimInstance -ClassName Win32_Service -Filter "Name = '$component'"
      if ($service.State -ne "Running") {
        Start-Service $component
      }
    } -AsJob
  }

  $jobs | Receive-Job -Wait
}
Wait-ConsulService $hiscox_environment "forms" $consul_address

$jobs | Receive-Job -Wait
