$ErrorActionPreference = "Stop"
. "$PSScriptRoot\functions.ps1"

$consul_address = $env:bamboo_consul_address
if ($env:bamboo_hiscox_environment) {
  $hiscox_environment = $env:bamboo_hiscox_environment
} else {
  $hiscox_environment = $env:bamboo_deploy_environment
}

$integration_address = Get-NodeAddress $hiscox_environment "integration" $consul_address | Select-Object -First 1
$integration_node = Get-NodeName $hiscox_environment "integration" $consul_address "azure.hiscox.com" | Select-Object -First 1

Write-Host "Running integration-db scripts on ${integration_node}"
$session = New-PSSession -ComputerName $integration_node
Invoke-Command -Session $session -FilePath "$PSScriptRoot\functions.ps1"
Invoke-Command -Session $session -ScriptBlock {
  $dateTime = Get-Date -UFormat "%Y%m%d%H%M"
  $logFile = "C:\programs\tomcat\apache-tomcat-8.0.18\integration\logs\imports_${dateTime}.log"
  $integration_address = $using:integration_address
  $integration_host = $integration_address.Split(":")[0]
  $integration_port = $integration_address.Split(":")[1]
  DB-ExecuteSQLGoalsForWebDB $integration_host $integration_port "static" "drop" "C:\programs\tomcat\apache-tomcat-8.0.18\integration\db" $logFile
  DB-ExecuteSQLGoalsForWebDB $integration_host $integration_port "live" "drop" "C:\programs\tomcat\apache-tomcat-8.0.18\integration\db" $logFile
}
