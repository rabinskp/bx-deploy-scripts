$ErrorActionPreference = "Stop"
. "$PSScriptRoot\functions.ps1"

$consul_address = $env:bamboo_consul_address

if ($env:bamboo_hiscox_environment) {
  $hiscox_environment = $env:bamboo_hiscox_environment
} else {
  $hiscox_environment = $env:bamboo_deploy_environment
}

$components = @("integration")

foreach ($component in $components) {
    Write-Host "Copy .properties on ${component}"

    Get-NodeName $hiscox_environment $component $consul_address "azure.hiscox.com" | ForEach-Object {
        $session = New-PSSession -ComputerName $_
        Invoke-Command -Session $session -ScriptBlock {
          if (Test-Path "C:\programs\tomcat\apache-tomcat-8.0.18\integration\integration") {
              Write-Host "Copying properties files..."
              Set-Location "C:\programs\tomcat"
              Copy-Item "C:\programs\tomcat\apache-tomcat-8.0.18\integration\integration\*" -Destination "C:\programs\tomcat\apache-tomcat-8.0.18\integration\lib" -Recurse -Force
          } else {
              Write-Host "Properties were not copied - location wasn't found"
          }
        }
    }
}