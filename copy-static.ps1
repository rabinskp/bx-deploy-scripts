$ErrorActionPreference = "Stop"
. "$PSScriptRoot\functions.ps1"

$consul_address = $env:bamboo_consul_address
if ($env:bamboo_hiscox_environment) {
  $hiscox_environment = $env:bamboo_hiscox_environment
} else {
  $hiscox_environment = $env:bamboo_deploy_environment
}

$portal_name = Get-NodeName $hiscox_environment "portal" $consul_address "azure.hiscox.com" | Select-Object -First 1
$portal_name_secondary = @(Get-NodeName $hiscox_environment "portal" $consul_address "azure.hiscox.com" | Select-Object -Skip 1)

$portal_session = New-PSSession -ComputerName $portal_name
Invoke-Command -Session $portal_session -ScriptBlock {
  Set-Location "C:\programs\tomcat"
  if (Test-Path ".\statics.7z") {
    Remove-Item ".\statics.7z" -Force
  }
  & 7z.exe a statics.7z .\statics
}
Copy-Item -Path "C:\programs\tomcat\statics.7z" -FromSession $portal_session
foreach ($portal_secondary in $portal_name_secondary) {
  Write-Host "Copying statics to ${portal_secondary}"
  $portal_secondary_session = New-PSSession -ComputerName $portal_secondary
  Copy-Item ".\statics.7z" -Destination "C:\programs\tomcat" -ToSession $portal_secondary_session -Force
  Invoke-Command -Session $portal_secondary_session -ScriptBlock {
    $service = Get-CimInstance -ClassName Win32_Service -Filter "Name = 'portal'"
    if ($service.State -ne "Stopped") {
      Stop-Process -Id $service.ProcessId -Force
    }
    Set-Location "C:\programs\tomcat"
    if (Test-Path ".\statics") {
      Remove-Item ".\statics" -Recurse -Force
    }
    & 7z.exe x .\statics.7z
  }
}