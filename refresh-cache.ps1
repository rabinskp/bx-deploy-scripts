$ErrorActionPreference = "Stop"
. "$PSScriptRoot\functions.ps1"

$consul_address = $env:bamboo_consul_address
if ($env:bamboo_hiscox_environment) {
  $hiscox_environment = $env:bamboo_hiscox_environment
} else {
  $hiscox_environment = $env:bamboo_deploy_environment
}

$components = @("integration")
$jobs = @()
$dateTime = Get-Date -UFormat "%Y%m%d%H%M"
$logFile = "C:\programs\tomcat\apache-tomcat-8.0.18\integration\logs\refreshCache_${dateTime}.log"

foreach ($component in $components) {
  switch ($component) {
    "integration" {
      Write-Host "Refreshing application cache..."
      Get-NodeAddress $hiscox_environment $component $consul_address | ForEach-Object {
        Write-Host "Address ${_}"
        $jobs += ClearDAOCache $_
        $jobs += ClearRepositoryCache $_
        $jobs += ClearBAPCache $_
      }
    }
    Default {}
  }
}

$jobs | Receive-Job -Wait